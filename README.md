[![pipeline status](https://gitlab.wilging.org/nwilging/laravel-search-filtering-middleware/badges/master/pipeline.svg)](https://gitlab.wilging.org/nwilging/laravel-search-filtering-middleware/commits/master)

# Installation

To use this package in your project, you must first add it to your repositories configuration:
```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.wilging.org/nwilging/laravel-search-filtering-middleware.git"
    }
]
```

Then require it in your project:
```
composer require nwilging/laravel-search-filtering-middleware
```

# Usage

## Middleware

The middleware is responsible for parsing the query string into a usable search/filter/expansion. You may
include the middleware in any way you wish.

Modify `app/Http/Kernel.php`

As global middleware:
```php
protected $middleware = [
    ...
    Nwilging\LaravelSearchMiddleware\Middleware\SearchFilterMiddleware::class,
    Nwilging\LaravelSearchMiddleware\Middleware\ExpandRelationshipsMiddleware::class,
    ...
]
```

As part of a group:
```php
protected $middlewareGroups = [
    'group' => [
        ...
        Nwilging\LaravelSearchMiddleware\Middleware\SearchFilterMiddleware::class,
        Nwilging\LaravelSearchMiddleware\Middleware\ExpandRelationshipsMiddleware::class,
        ...
    ]
]
```

As Route middleware:
```php
protected $routeMiddleware = [
    ...
    'search' => Nwilging\LaravelSearchMiddleware\Middleware\SearchFilterMiddleware::class,
    'expand' => Nwilging\LaravelSearchMiddleware\Middleware\ExpandRelationshipsMiddleware::class,
    ...
]
```

## Traits

You may apply traits to your controller to allow you to easily access the parsed query strings to pass to
a repository, query, etc. (May I suggest using https://gitlab.wilging.org/nwilging/eloquent-repositories)

In your controller, use the following traits:

```php
use Nwilging\LaravelSearchMiddleware\Traits\WithSearchAndFilter;
use Nwilging\LaravelSearchMiddleware\Traits\WithRelationshipExpansion;
...
class MyController extends Controller
{
    use WithSearchAndFilter, WithRelationshipExpansion;
    ...
    
    public function myMethod(Request $request)
    {
        $expanded = $this->expand($request);
        $refined = $this->refine($request);
    }
}
```

# API Usage

This middleware is designed to be used in a Laravel API. It provides the ability to search, filter, and
expand model relationships in the query string of a request.

## Filtering

Filtering is the simplest form of searching. It does a direct comparison on a key to a given value (=).
You may add it to the query string like so:

`https://yourapi.org/path/to/endpoint?filter[key]=value`

This will effectively give you the ability to do `where[key] = value` in a query.

## Searching

Searching is a bit more complex. You are able to use the following qualifiers:

```
eq          Equals (=)
neq         Not Equals (!=)
gt          Greater Than (>)
gte         Greater Than-Equal To (>=)
lt          Less Than (<)
lte         Less Than-Equal To (<=)
in          In Array of Values
notin       Not In Array of Values
Like        Like comparison (accepts wildcards as *)
Between     Between comparison between two values
```

Here is an example search query using `eq`:

`https://yourapi.org/path/to/endpoint?search[column]=eq,value`

`https://yourapi.org/path/to/endpoint?search[name]=eq,Nick`

Here is an example using `in`:

`https://yourapi.org/path/to/endpoint?search[column]=in,value,value2,value3,etc`

`https://yourapi.org/path/to/endpoint?search[id]=in,1,3,5,7`

## Expanding Relationships

You may expand relationships on models similarly to how you would use `with` programmatically.

`https://yourapi.org/path/to/endpoint?expand[relationshipMethod]=*`

Note: You should always use `*`. Support for expanding only certain columns is not yet implemented and will
throw an exception.