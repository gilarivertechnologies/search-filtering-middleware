<?php

namespace Nwilging\LaravelSearchMiddleware\Tests;

use Illuminate\Contracts\Console\Kernel;

/**
 * Trait CreatesApplication
 * @package Nwilging\LaravelSearchMiddleware\Tests
 */
trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();
        return $app;
    }
}
