<?php
/**
 * Test for SearchFilterMiddleware
 */

namespace Nwilging\LaravelSearchMiddleware\Tests\Unit\Middleware;

use Illuminate\Http\Request;
use Nwilging\LaravelSearchMiddleware\Exceptions\MalformedSearchTermException;
use Nwilging\LaravelSearchMiddleware\Middleware\SearchFilterMiddleware;
use Nwilging\LaravelSearchMiddleware\Tests\TestCase;

/**
 * Class SearchFilterMiddlewareTest
 * @package Nwilging\LaravelSearchMiddleware\Tests\Unit\Middleware
 */
class SearchFilterMiddlewareTest extends TestCase
{
    /**
     * @var SearchFilterMiddleware
     */
    protected $middleware;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var \Closure
     */
    protected $closure;

    public function setUp()
    {
        parent::setUp();

        $this->middleware = new SearchFilterMiddleware();
        $this->request = new Request();
        $this->closure = function () {};
    }

    /**
     * @return array
     */
    public function searchTypesProvider()
    {
        return [
            ['eq', '='],
            ['neq', '!='],
            ['lt', '<'],
            ['lte', '<='],
            ['gt', '>'],
            ['gte', '>='],
            ['like', 'like'],
        ];
    }

    /**
     * @return array
     */
    public function searchTypesProviderContainers()
    {
        return [
            ['in', 'in', '1', [1]],
            ['in', 'in', '1,2,3', [1,2,3]],
            ['notin', 'not in', '1', [1]],
            ['notin', 'not in', '1,2,3', [1,2,3]],
        ];
    }

    /**
     * @return array
     */
    public function searchTypeLikeProvider()
    {
        return [
            ['helloworld', 'helloworld'],
            ['*helloworld', '%helloworld'],
            ['**helloworld', '%*helloworld'],
            ['helloworld*', 'helloworld%'],
            ['helloworld**', 'helloworld*%'],
            ['*helloworld*', '%helloworld%'],
            ['*hello*world', '%hello*world'],
        ];
    }

    public function testMiddlewareWraps()
    {
        $closure = function (Request $request) {
            $request->wrapped = 1;
            return $request;
        };

        $response = $this->middleware->handle($this->request, $closure);
        $this->assertInstanceOf(Request::class, $response);
        $this->assertEquals(1, $response->wrapped);
    }

    public function testWithoutParameters()
    {
        $this->middleware->handle($this->request, $this->closure);
        $this->assertEmpty($this->request->query->all());
    }

    public function testFilterParamOnlyAlterIfArray()
    {
        $this->request->query->set('filter', 'string');
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('filter', $query);
    }

    public function testFilterParamEqualityStructure()
    {
        $this->request->query->set('filter', ['key' => 'value']);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(2, $query);
        $this->assertArrayHasKey('filter', $query);
        $this->assertArrayHasKey('refine', $query);
        $this->assertCount(1, $query['refine']);
        $this->assertEquals(['key', '=', 'value'], $query['refine'][0]);
    }

    /**
     * @depends testFilterParamEqualityStructure
     * @throws \Nwilging\LaravelSearchMiddleware\Exceptions\MalformedSearchTermException
     */
    public function testFilterParamUrlEncode()
    {
        $this->request->query->set('filter', ['key' => urlencode("/\"'_><?,.+`")]);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertEquals(['key', '=', "/\"'_><?,.+`"], $query['refine'][0]);
    }

    public function testSearchDoesNotAlterNonArrays()
    {
        $this->request->query->set('search', 'string');
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('search', $query);
    }

    public function testSearchParamMissingValuesFails()
    {
        $this->expectException(MalformedSearchTermException::class);
        $this->request->query->set('search', ['key' => 'ne']);
        $this->middleware->handle($this->request, $this->closure);
    }

    public function testSearchParamInvalidQualifier()
    {
        $this->expectException(MalformedSearchTermException::class);
        $this->request->query->set('search', ['key' => 'eqq,invalid']);
        $this->middleware->handle($this->request, $this->closure);
    }

    public function testSearchParamEqualityStructure()
    {
        $this->request->query->set('search', ['key' => 'eq,value']);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(2, $query);
        $this->assertArrayHasKey('search', $query);
        $this->assertArrayHasKey('refine', $query);
        $this->assertCount(1, $query['refine']);
        $this->assertEquals(['key', '=', 'value'], $query['refine'][0]);
    }

    public function testSearchOrderBy()
    {
        $this->request->query->set('order_by', ['column' => 'ASC']);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('order_by', $query);
        $this->assertCount(1, $query['order_by']);
        $this->assertEquals(['column' => 'ASC'], $query['order_by']);
    }

    public function testSearchMultipleOrderBy()
    {
        $this->request->query->set('order_by', [
            'column' => 'ASC',
            'another' => 'DESC'
        ]);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('order_by', $query);
        $this->assertCount(2, $query['order_by']);
        $this->assertEquals([
            'column' => 'ASC',
            'another' => 'DESC'
        ], $query['order_by']);
    }

    public function testSearchLimit()
    {
        $this->request->query->set('limit', 10);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('limit', $query);
        $this->assertEquals(10, $query['limit']);
    }

    public function testSearchLimitAsString()
    {
        $this->request->query->set('limit', '100');
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertCount(1, $query);
        $this->assertArrayHasKey('limit', $query);
        $this->assertEquals(100, $query['limit']);
    }

    /**
     * @depends testSearchParamEqualityStructure
     * @dataProvider searchTypesProvider
     * @param $operator
     * @param $translated
     * @throws MalformedSearchTermException
     */
    public function testSearchParamComparisonTypesSuccess($operator, $translated)
    {
        $value = $operator . ',value';
        $this->request->query->set('search', ['key' => $value]);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertEquals(['key', $translated, 'value'], $query['refine'][0]);
    }

    /**
     * @depends testSearchParamEqualityStructure
     * @throws MalformedSearchTermException
     */
    public function testSearchParamComparisonTypesBetweenSuccess()
    {
        $value = 'between,value,other-value';
        $this->request->query->set('search', ['key' => $value]);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertEquals(['key', '>=', 'value'], $query['refine'][0]);
        $this->assertEquals(['key', '<=', 'other-value'], $query['refine'][1]);
    }

    /**
     * @depends testSearchParamEqualityStructure
     * @dataProvider searchTypesProviderContainers
     * @param $operator
     * @param $translatedOperator
     * @param $value
     * @param $translatedValue
     * @throws MalformedSearchTermException
     */
    public function testSearchParamContainerSuccess($operator, $translatedOperator, $value, $translatedValue)
    {
        $this->request->query->set('search', ['key' => $operator . ',' . $value]);
        $this->middleware->handle($this->request, $this->closure);

        $query = $this->request->query->all();
        $this->assertEquals(['key', $translatedOperator, $translatedValue], $query['refine'][0]);
    }

    /**
     * @depends testSearchParamEqualityStructure
     * @dataProvider searchTypeLikeProvider
     * @param $value
     * @param $translated
     * @throws MalformedSearchTermException
     */
    public function testSearchParamLike($value, $translated)
    {
        $this->request->query->set('search', ['key' => 'like,' . $value]);
        $this->middleware->handle($this->request, $this->closure);
        $query = $this->request->query->all();
        $this->assertEquals(['key', 'like', $translated], $query['refine'][0]);
    }
}
