<?php
/**
 * Expand Invalid Columns Exception
 */

namespace Nwilging\LaravelSearchMiddleware\Exceptions;

/**
 * Class ExpandInvalidColumnsException
 * @package Nwilging\LaravelSearchMiddleware\Exceptions
 */
class ExpandInvalidColumnsException extends \Exception
{
    protected static $exceptionMessage = 'Column type of [%s] is invalid.';

    /**
     * ExpandInvalidColumnsException constructor.
     * @param string $column
     */
    public function __construct(string $column)
    {
        parent::__construct(sprintf(static::$exceptionMessage, $column), 400);
    }
}
