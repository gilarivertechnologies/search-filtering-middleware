<?php
/**
 * Malformed Search Term Exception
 */

namespace Nwilging\LaravelSearchMiddleware\Exceptions;

/**
 * Class MalformedSearchTermException
 * @package Nwilging\LaravelSearchMiddleware\Exceptions
 */
class MalformedSearchTermException extends \Exception
{
    protected static $exceptionMessage = 'Search term [%s] is invalid: %s';

    /**
     * MalformedSearchTermException constructor.
     * @param string $searchTerm
     * @param string $message
     */
    public function __construct(string $searchTerm, string $message)
    {
        parent::__construct(sprintf(static::$exceptionMessage, $searchTerm, $message), 400);
    }
}
