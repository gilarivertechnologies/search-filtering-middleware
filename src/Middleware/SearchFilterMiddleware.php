<?php
/**
 * Middleware that parses query strings into searchable/filterable queries
 */

namespace Nwilging\LaravelSearchMiddleware\Middleware;
use Nwilging\LaravelSearchMiddleware\Exceptions\MalformedSearchTermException;

/**
 * Class SearchFilterMiddleware
 * @package Nwilging\LaravelSearchMiddleware\Middleware
 */
class SearchFilterMiddleware
{
    protected $searchTypes = [
        'gt' => '>',
        'gte' => '>=',
        'lt' => '<',
        'lte' => '<=',
        'eq' => '=',
        'neq' => '!=',
        'in' => 'in',
        'notin' => 'not in',
        'like' => 'like',
        'between' => 'between'
    ];

    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     * @throws MalformedSearchTermException
     */
    public function handle($request, \Closure $next)
    {
        $refine = [];
        $orderBy = [];
        $limit = null;

        if ($order = $request->query('order_by')) {
            if (is_array($order)) {
                foreach ($order as $column => $direction) {
                    $orderBy[$column] = $direction;
                }
            }
        }

        if ($limitFilter = $request->query('limit')) {
            if (!is_array($limitFilter) && !is_null($limitFilter)) {
                $limit = (int) $limitFilter;
            }
        }

        if ($filters = $request->query('filter')) {
            if (is_array($filters)) {
                foreach ($filters as $key => $value) {
                    $refine[] = [$key, '=', urldecode($value)];
                }
            }
        }

        if ($search = $request->query('search')) {
            if (is_array($search)) {
                foreach ($search as $key => $term) {
                    $parts = explode(',', $term, 2);
                    if (empty($parts[1])) {
                        throw new MalformedSearchTermException($key, 'Missing a value.');
                    }

                    $searchTerm = $parts[1];
                    if (!$searchType = $this->computeSearchType($parts[0])) {
                        throw new MalformedSearchTermException($key,sprintf('Invalid qualifier (%s)', $searchTerm));
                    }

                    if ($searchType === 'in' || $searchType === 'not in' || $searchType === 'between') {
                        $searchTerm = explode(',', $searchTerm);
                    }

                    if ($searchType == 'like') {
                        if ($this->startsWith('*', $searchTerm)) {
                            $searchTerm = '%' . substr($searchTerm, 1);
                        }
                        if ($this->endsWith('*', $searchTerm)) {
                            $searchTerm = substr($searchTerm, 0, -1) . '%';
                        }
                    }

                    if ($searchType === 'between') {
                        $refine[] = [$key, '>=', urldecode($searchTerm[0])];
                        $refine[] = [$key, '<=', urldecode($searchTerm[1])];
                    } else {
                        $refine[] = [$key, $searchType, (is_array($searchTerm) ? array_map('urldecode', $searchTerm) : urldecode($searchTerm))];
                    }
                }
            }
        }

        if ($refine) $request->query->set('refine', $refine);
        if ($orderBy) $request->query->set('order_by', $orderBy);
        if ($limit) $request->query->set('limit', $limit);

        return $next($request);
    }

    /**
     * @param string $searchType
     * @return bool|string
     */
    protected function computeSearchType(string $searchType)
    {
        if (!array_key_exists($searchType, $this->searchTypes)) {
            return false;
        }
        return $this->searchTypes[$searchType];
    }

    /**
     * @param string $needle
     * @param string $haystack
     * @return bool
     */
    protected function startsWith(string $needle, string $haystack)
    {
        $len = strlen($needle);
        return (substr($haystack, 0, $len) === $needle);
    }

    /**
     * @param string $needle
     * @param string $haystack
     * @return bool
     */
    protected function endsWith(string $needle, string $haystack)
    {
        $len = strlen($needle);
        return $len === 0 || (substr($haystack, -$len) === $needle);
    }
}
