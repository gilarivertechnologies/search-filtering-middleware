<?php
/**
 * Expands relationships from query string
 */

namespace Nwilging\LaravelSearchMiddleware\Middleware;

use Nwilging\LaravelSearchMiddleware\Exceptions\ExpandInvalidColumnsException;

/**
 * Class ExpandRelationshipsMiddleware
 * @package Nwilging\LaravelSearchMiddleware\Middleware
 */
class ExpandRelationshipsMiddleware
{
    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     * @throws ExpandInvalidColumnsException
     */
    public function handle($request, \Closure $next)
    {
        $with = [];

        if ($toExpand = $request->query('expand')) {
            if (is_array($toExpand)) {
                foreach ($toExpand as $field => $cols) {
                    if ($cols !== '*') {
                        throw new ExpandInvalidColumnsException($cols);
                    }
                    $with[] = $field;
                }
            }
        }

        if ($with) $request->query->set('with', $with);

        return $next($request);
    }
}
