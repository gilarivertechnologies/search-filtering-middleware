<?php
/**
 * Expands relationships
 */

namespace Nwilging\LaravelSearchMiddleware\Traits;

use Illuminate\Http\Request;

/**
 * Trait WithRelationshipExpansion
 * @package Illuminate\Support\Traits
 */
trait WithRelationshipExpansion
{
    /**
     * @param Request $request
     * @return array|null|string
     */
    protected function expand(Request $request)
    {
        return $request->input('with', []);
    }
}
