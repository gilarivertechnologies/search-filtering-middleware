<?php
/**
 * Applies search and filter middleware (refine)
 */

namespace Nwilging\LaravelSearchMiddleware\Traits;
use Illuminate\Http\Request;

/**
 * Trait WithSearchAndFilter
 * @package Nwilging\LaravelSearchMiddleware\Traits
 */
trait WithSearchAndFilter
{
    /**
     * @param Request $request
     * @return array
     */
    protected function refine(Request $request): array
    {
        return $request->input('refine', []);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function orderBy(Request $request): array
    {
        return $request->input('order_by', []);
    }

    /**
     * @param Request $request
     * @return int|null
     */
    protected function limit(Request $request): ?int
    {
        return $request->input('limit', null);
    }
}
